const fs = require('fs');
const path = require('path');
const express = require('express');
const morgan = require('morgan')
const app = express();

const {filesRouter} = require('./filesRouter.js');

const errorHandler = (err, req, res, next) => {
    console.error(err);
    res.status(err.statusCode).send({
        "message": err.message
    });
}

const logger = fs.createWriteStream(path.join(__dirname, 'history.log'), {flags: 'a'});

const start = () => {
    try {
        if (!fs.existsSync('files')) {
            fs.mkdirSync('files');
        }

        if (!fs.existsSync('passwords.json')) {
            fs.writeFileSync('passwords.json', '{ }');
        }

        app.listen(8080);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

app.use(express.json());
app.use(morgan('combined', {stream: logger}));
app.use('/api/files', filesRouter);
app.use(errorHandler);

start();
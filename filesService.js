// requires...
const fs = require('fs');
const path = require('path');

// constants...
const DEFAULT_PATH = path.join(__dirname, 'files', '/');
const passwordFile = path.join(__dirname, 'passwords.json');
const extensionsList = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

const getFilePath = (fileName) => path.join(DEFAULT_PATH, String(fileName));
const getPasswords = () => JSON.parse(fs.readFileSync(passwordFile, 'utf-8'));
const checkExtension = (fileName) => extensionsList.includes(fileName.slice(fileName.lastIndexOf('.') + 1));


const createFile = (req, res, next) => {
    const {filename: fileName, content: fileContent} = req.body;
    const filePath = getFilePath(fileName);
    const password = req.query.password;
    const passwords = getPasswords();


    // if (!password) {
    //     next({
    //         message: "Password is missing",
    //         statusCode: 400
    //     });
    //     return;
    // }

    if (!fileName) {
        next({
            message: "Please specify 'filename' parameter",
            statusCode: 400
        });
        return;
    }

    if (!fileContent) {
        next({
            message: "Please specify 'content' parameter",
            statusCode: 400
        });
        return;
    }

    if (!checkExtension(fileName)) {
        next({
            message: "Extension type is not valid",
            statusCode: 400
        });
        return;
    }

    if (fs.existsSync(filePath)) {
        next({
            message: 'Such file already exists',
            statusCode: 400
        });
        return;
    }

    fs.writeFile(filePath, fileContent, () => {
        passwords[`${fileName}`] = { password }
        fs.writeFileSync(passwordFile, JSON.stringify(passwords));
        res.status(200).send({
            "message": "File created successfully"
        });
    });
}

const getFiles = (req, res, next) => {
    fs.readdir(DEFAULT_PATH, (err, files) => {
        if (err) {
            next({
                message: 'Server error',
                statusCode: 500
            });
            return;
        }
        res.status(200).send({
            "message": "Success",
            "files": files
        });
    });
}

const getFile = (req, res, next) => {
    const fileName = req.params.filename;
    const filePath = getFilePath(fileName);
    const password = req.query.password;
    const passwords = getPasswords();

    // if (!password) {
    //     next({
    //         message: "Password is missing",
    //         statusCode: 400
    //     });
    //     return;
    // }
    //
    // if (password !== passwords[`${fileName}`].password) {
    //     next({
    //         message: 'Wrong password provided',
    //         statusCode: 400
    //     });
    //     return;
    // }

    if (!fs.existsSync(filePath)) {
        next({
            message: `No file with '${fileName}' filename found`,
            statusCode: 400
        });
        return;
    }

    res.status(200).send({
        "message": "Success",
        "filename": path.basename(filePath),
        "content": fs.readFileSync(filePath).toString(),
        "extension": path.extname(filePath).slice(1),
        "uploadedDate": fs.statSync(filePath).mtime
    });
}

const changeFile = (req, res, next) => {
    const {filename: fileName, content: fileContent} = req.body;
    const filePath = getFilePath(fileName);
    const password = req.query.password;
    const passwords = getPasswords();

    // if (!password) {
    //     next({
    //         message: "Password is missing",
    //         statusCode: 400
    //     });
    //     return;
    // }
    //
    // if (password !== passwords[`${fileName}`].password) {
    //     next({
    //         message: 'Wrong password provided',
    //         statusCode: 400
    //     });
    //     return;
    // }

    if (!fs.existsSync(filePath)) {
        next({
            message: `No file with '${fileName}' filename found`,
            statusCode: 400
        });
        return;
    }

    fs.writeFile(path.join(DEFAULT_PATH, fileName), fileContent, () => {
        res.status(200).send({
            "message": "File successfully modified"
        });
    });
}

const deleteFile = (req, res, next) => {
    const fileName = req.params.filename;
    const filePath = getFilePath(fileName);
    const passwords = getPasswords();

    if (!fs.existsSync(filePath)) {
        next({
            message: `No file with '${fileName}' filename found`,
            statusCode: 400
        });
        return;
    }

    fs.unlink(filePath, () => {
        delete passwords[`${fileName}`];
        fs.writeFileSync(passwordFile, JSON.stringify(passwords));
        res.status(200).send({
            message: "File successfully deleted"
        });
    });
}


module.exports = {
    createFile,
    getFiles,
    getFile,
    changeFile,
    deleteFile
}